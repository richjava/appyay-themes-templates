import Link from 'next/link';

const SimpleBlogTeaser = () => {
  return (
    <section className="sbt">
      <p>
        Here is a blurb for the BlogTeaser component that will come from the
        CMS.
      </p>
      <Link href="/blog">
        <a>Go to Blog</a>
      </Link>
      <style jsx>{`
        .sbt {
          background: #ccc;
        }
      `}</style>
    </section>
  );
};

export default SimpleBlogTeaser;
