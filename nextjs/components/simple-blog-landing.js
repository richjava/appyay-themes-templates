import Link from 'next/link';

const SimpleBlogLanding = () => {
  return (
    <section className="sbl">
     <p>Here is a blurb for the Blog page landing section that will come from the CMS.</p>
     <Link href="/posts/1">
        <a>This is a blog entry</a>
      </Link>
      <style jsx>{``}</style>
    </section>
  );
};

export default SimpleBlogLanding;
